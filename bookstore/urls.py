from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import RedirectView
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny
from rest_framework.authentication import SessionAuthentication


admin.site.site_title = 'Bookstore Administration'
admin.site.site_header = 'Bookstore Administration'


schema_view = get_schema_view(
   openapi.Info(
      title="Bookstore API V1",
      default_version='v1',
   ),
   public=False,
   permission_classes=(AllowAny,),
   authentication_classes=(SessionAuthentication,),
)

urlpatterns = [
    re_path(r'docs/(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'docs$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),

    path('', RedirectView.as_view(url='/docs'), name='go-to-docs'),
    path('admin/', admin.site.urls),
    path('v1/', include('apps.api.api_v1.urls', namespace='api_v1')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)