from django.urls import path, include


app_name = 'api_v1'

urlpatterns = [
   path('book/', include('apps.book.urls', namespace='book')),
   path('member/', include('apps.member.urls', namespace='member')),
]