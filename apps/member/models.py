from decimal import Decimal

from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator
from django.db import models

from ..utils.models import CommonInfoModel


class TransactionType(models.TextChoices):
    REDEEM = 'redeem'
    BUY = 'buy'


class User(AbstractUser, CommonInfoModel):
    username = models.CharField(max_length=50, null=True, unique=True)
    first_name = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=200, blank=True, null=True)
    point = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], default=Decimal('0.00'))
    money = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], default=Decimal('0.00'))

    def __str__(self):
        return self.username


class Transaction(CommonInfoModel):
    type = models.CharField(max_length=100, choices=TransactionType.choices)
    user = models.ForeignKey(
        'member.User',
        related_name='trasactions',
        on_delete=models.CASCADE
    )
    books = models.ManyToManyField(
        'book.Book',
        through='TransactionBook',
        related_name='trasactions',
        blank=True
    )
    total_price = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], null=True)
    total_point_used = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], null=True)
    total_point_recieved = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], null=True)
    ratio = models.ForeignKey(
        'member.Ratio',
        related_name='trasactions',
        on_delete=models.CASCADE,
        null=True, blank=True
    )


class Ratio(CommonInfoModel):
    baht_to_point_ratio = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], null=True)
    point_to_baht_ratio = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], null=True)
    active = models.BooleanField(default=True)


class TransactionBook(CommonInfoModel):
    transaction = models.ForeignKey(
        'member.Transaction',
        on_delete=models.CASCADE,
        related_name='transaction_book'
    )
    book = models.ForeignKey(
        'book.Book',
        on_delete=models.CASCADE,
        related_name='transaction_book'
    )
    amount = models.IntegerField()