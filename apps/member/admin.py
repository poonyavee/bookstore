from django.contrib import admin

from .models import (
    User,
    Ratio,
    Transaction,
)

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('uid', 'username', 'first_name', 'last_name', 'point', 'money', 'created_at', 'updated_at')

    def get_queryset(self, request):
        return super().get_queryset(request).exclude(is_active=False)


@admin.register(Ratio)
class RatioAdmin(admin.ModelAdmin):
    list_display = ('uid', 'baht_to_point_ratio', 'point_to_baht_ratio', 'created_at', 'updated_at')


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('uid', 'type', 'total_price', 'total_point_used', 'total_point_recieved', 'updated_at')
