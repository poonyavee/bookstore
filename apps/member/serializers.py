from django.contrib.auth import authenticate

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import TransactionType, User
from ..book.models import Book


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(username=attrs['username'], password=attrs['password'])
        if not user:
            raise ValidationError('Your authentication information is incorrect. Please try again.')
        return user


class MemberCreateSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    point = serializers.DecimalField(required=False, decimal_places=2, max_digits=12)
    money = serializers.DecimalField(required=False, decimal_places=2, max_digits=12)

    def validate_username(self, value):
        if value is None:
            return value
        
        user = User.objects.filter(username=value).exists()
        if user:
            raise ValidationError('This username is not available.')

        return value


class MemberUpdateSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    point = serializers.DecimalField(required=False, decimal_places=2, max_digits=12)
    money = serializers.DecimalField(required=False, decimal_places=2, max_digits=12)

    def validate_username(self, value):
        if value is None:
            return value
        
        user = User.objects.filter(username=value).exists()
        if user:
            raise ValidationError('This username is not available.')

        return value


class MemberDetailSerializer(serializers.Serializer):
    id = serializers.UUIDField(source='uid')
    username = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    point = serializers.DecimalField(decimal_places=2, max_digits=12)
    money = serializers.DecimalField(decimal_places=2, max_digits=12)


class RatioCreateSerializer(serializers.Serializer):
    baht_to_point_ratio = serializers.DecimalField(decimal_places=2, max_digits=12)
    point_to_baht_ratio = serializers.DecimalField(decimal_places=2, max_digits=12)


class RatioDetailSerializer(serializers.Serializer):
    baht_to_point_ratio = serializers.DecimalField(decimal_places=2, max_digits=12)
    point_to_baht_ratio = serializers.DecimalField(decimal_places=2, max_digits=12)
    active = serializers.BooleanField()


class TransactionCreateSerializer(serializers.Serializer):
    type = serializers.ChoiceField(choices=TransactionType.choices)
    books = serializers.ListField(child=serializers.JSONField())

    def validate(self, attrs):
        user = self.context['request'].user
        attrs['user'] = user
        return attrs

    def validate_books(self, value):
        if value is None:
            return value

        books = []
        for book_obj in value:
            book = Book.objects.filter(uid=book_obj['id'], deleted_at__isnull=True).first()
            books.append({
                'book': book,
                'amount': book_obj.get('amount', '1'),
            })
        if not books:
            raise ValidationError('Book is not exists.')

        return books


class TransactionDetailSerializer(serializers.Serializer):
    id = serializers.UUIDField(source='uid')
    type = serializers.CharField()
    user = MemberDetailSerializer()
    books = serializers.SerializerMethodField()
    total_price = serializers.DecimalField(decimal_places=2, max_digits=12)
    total_point_recieved = serializers.DecimalField(decimal_places=2, max_digits=12)
    total_point_used = serializers.DecimalField(decimal_places=2, max_digits=12)
    ratio = RatioDetailSerializer()
    
    def get_books(self, obj):
        return [{'name': obj.book.name, 'price': obj.book.price, 'amount': obj.amount} for obj in obj.transaction_book.all()]