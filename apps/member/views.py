from decimal import Decimal

from django.contrib.auth import login, logout

from rest_framework import status
from rest_framework.generics import (
    GenericAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .models import (
    User,
    Ratio,
    Transaction,
    TransactionBook,
    TransactionType,
)
from .serializers import (
    UserLoginSerializer,
    MemberCreateSerializer,
    MemberUpdateSerializer,
    MemberDetailSerializer,
    RatioCreateSerializer,
    RatioDetailSerializer,
    TransactionCreateSerializer,
    TransactionDetailSerializer
)
from ..utils.pagination import CustomPageNumberPagination


class UserLoginAPI(GenericAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        login(request, serializer.validated_data)
        content = {
            'username': request.user.username
        }
        return Response(content)


class UserLogoutAPI(GenericAPIView):
    pagination_class = None

    def get(self, request, *args, **kwargs):
        logout(request)
        return Response({ 'logout': 'done' })


class MemberCreateAPI(ListCreateAPIView):
    queryset = User.objects.all().exclude(is_active=False)
    pagination_class = CustomPageNumberPagination
    
    def get_serializer_class(self):
        method = self.request.method
        if method == 'POST':
            return MemberCreateSerializer
        return MemberDetailSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        password = data.pop('password', None)

        member = User.objects.create(**data)
        if password:
            member.set_password(password)
            member.save()
        return Response({'id': member.uid}, status=status.HTTP_201_CREATED)


class MemberDetailAPI(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all().exclude(is_active=False)
    lookup_field = 'uid'

    def get_serializer_class(self):
        method = self.request.method
        if method in ['PUT', 'PATCH']:
            return MemberUpdateSerializer
        return MemberDetailSerializer
    
    def retrieve(self, request, *args, **kwargs):
        member = self.get_object()
        response = MemberDetailSerializer(member).data
        return Response(response, status=status.HTTP_200_OK)
    
    def update(self, request, *args, **kwargs):
        member = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        password = data.pop('password', None)
        
        for field, value in data.items():
            setattr(member, field, value)

        if password:
            member.set_password(password)
        member.save()

        response = MemberDetailSerializer(member).data
        return Response(response, status=status.HTTP_200_OK)
    
    def destroy(self, request, *args, **kwargs):
        member = self.get_object()
        member.is_active = False
        self.perform_destroy(member)
        return Response(status=status.HTTP_204_NO_CONTENT)


class RatioCreateAPI(ListCreateAPIView):
    queryset = Ratio.objects.all()
    pagination_class = CustomPageNumberPagination

    def get_serializer_class(self):
        method = self.request.method
        if method == 'POST':
            return RatioCreateSerializer
        return RatioDetailSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        previous_ratio = Ratio.objects.last()
        if previous_ratio:
            previous_ratio.active = False
            previous_ratio.save()
        ratio = Ratio.objects.create(**data)
        return Response({'id': ratio.uid}, status=status.HTTP_201_CREATED)


class TransactionCreateAPI(ListCreateAPIView):
    queryset = Transaction.objects.all()
    pagination_class = CustomPageNumberPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.order_by(
            '-created_at'
        ).select_related(
            'user',
            'ratio',
        ).prefetch_related(
            'books'
        )

    def get_serializer_class(self):
        method = self.request.method
        if method == 'POST':
            return TransactionCreateSerializer
        return TransactionDetailSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(user=request.user)
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        data['ratio'] = Ratio.objects.last()
        books = data.pop('books', None)
        transaction_type = data['type']
        if books:
            if transaction_type == TransactionType.BUY:
                total_price = 0
                for book_obj in books:
                    total_price += (Decimal(book_obj['book'].price) * Decimal(book_obj['amount']))
                
                if request.user.money:
                    balance = Decimal(request.user.money) - Decimal(total_price)
                    if balance >= Decimal('0.00'):
                        request.user.money = balance
                        point_recieved = Decimal(total_price) * Decimal(data['ratio'].baht_to_point_ratio)
                        request.user.point += point_recieved
                        request.user.save()
                        data['total_price'] = total_price
                        data['total_point_recieved'] = point_recieved
                    else:
                        return Response({ 'fail': 'money not enough' }, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({ 'fail': 'you not have money' }, status=status.HTTP_400_BAD_REQUEST)
            
            elif transaction_type == TransactionType.REDEEM:
                total_redeem_point = 0
                for book_obj in books:
                    total_redeem_point += (Decimal(book_obj['book'].price) * Decimal(data['ratio'].point_to_baht_ratio) * Decimal(book_obj['amount']))

                if request.user.point:
                    balance = Decimal(request.user.point) - Decimal(total_redeem_point)
                    if balance >= Decimal('0.00'):
                        request.user.point = balance
                        request.user.save()
                        data['total_point_used'] = total_redeem_point
                    else:
                        return Response({ 'fail': 'point not enough' }, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({ 'fail': 'you not have point' }, status=status.HTTP_400_BAD_REQUEST)

            transaction = Transaction.objects.create(**data)
            if books:
                transaction_books = []
                for book_obj in books:
                    transaction_books.append(
                        TransactionBook(
                            transaction=transaction,
                            book=book_obj['book'],
                            amount=book_obj['amount'],
                        )
                    )
                
                TransactionBook.objects.bulk_create(transaction_books)

            response = TransactionDetailSerializer(transaction).data
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response({ 'fail': 'money and point not enough' }, status=status.HTTP_400_BAD_REQUEST)
