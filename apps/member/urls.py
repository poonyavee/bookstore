from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import (
    MemberCreateAPI,
    MemberDetailAPI,
    RatioCreateAPI,
    UserLoginAPI,
    UserLogoutAPI,
    TransactionCreateAPI,
)


app_name = 'book'

router = DefaultRouter(trailing_slash=False)

urlpatterns = [
    path('member', MemberCreateAPI.as_view(), name='member-list'),
    path('member/<uuid:uid>', MemberDetailAPI.as_view(), name='member-detail'),
    path('login', UserLoginAPI.as_view(), name='member-login'),
    path('logout', UserLogoutAPI.as_view(), name='member-logout'),
    path('ratio', RatioCreateAPI.as_view(), name='ratio-create'),
    path('transaction', TransactionCreateAPI.as_view(), name='transaction'),
]
urlpatterns += router.urls