from django.contrib import admin

from .models import (
    Book,
    Author,
)

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('uid', 'name', 'price', 'get_author', 'created_at', 'updated_at')

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('author')

    def get_author(self, obj):
        return obj.author.first_name


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('uid', 'first_name', 'last_name', 'created_at', 'updated_at')