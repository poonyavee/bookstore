from rest_framework import status
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.response import Response

from .models import (
    Book,
    Author,
)
from .serializers import (
    BookCreateSerializer,
    BookUpdateSerializer,
    BookDetailSerializer,
    AuthorCreateSerializer,
    AuthorDetailSerializer,
)
from ..utils.pagination import CustomPageNumberPagination


class BookCreateAPI(ListCreateAPIView):
    queryset = Book.objects.all()
    pagination_class = CustomPageNumberPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.order_by(
            '-created_at'
        ).select_related(
            'author'
        )

    def get_serializer_class(self):
        method = self.request.method
        if method == 'POST':
            return BookCreateSerializer
        return BookDetailSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        book = Book.objects.create(**data)
        return Response({'id': book.uid}, status=status.HTTP_201_CREATED)


class BookDetailAPI(RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    lookup_field = 'uid'

    def get_serializer_class(self):
        method = self.request.method
        if method in ['PUT', 'PATCH']:
            return BookUpdateSerializer
        return BookDetailSerializer
    
    def retrieve(self, request, *args, **kwargs):
        self.queryset = self.get_queryset().select_related(
            'author'
        )
        book = self.get_object()
        response = self.get_serializer(book).data
        return Response(response, status=status.HTTP_200_OK)
    
    def update(self, request, *args, **kwargs):
        book = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        for field, value in data.items():
            setattr(book, field, value)

        book.save()

        response = BookDetailSerializer(book).data
        return Response(response, status=status.HTTP_200_OK)
    
    def destroy(self, request, *args, **kwargs):
        book = self.get_object()
        self.perform_destroy(book)
        return Response(status=status.HTTP_204_NO_CONTENT)


class AuthorCreateAPI(ListCreateAPIView):
    queryset = Author.objects.all()
    pagination_class = CustomPageNumberPagination

    def get_serializer_class(self):
        method = self.request.method
        if method == 'POST':
            return AuthorCreateSerializer
        return AuthorDetailSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        author = Author.objects.create(**data)
        return Response({'id': author.uid}, status=status.HTTP_201_CREATED)