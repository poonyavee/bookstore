from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models

from ..utils.models import CommonInfoModel


class Book(CommonInfoModel):
    name = models.CharField(max_length=200)
    price = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))])
    author = models.ForeignKey(
        'book.Author',
        related_name='books',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
    

class Author(CommonInfoModel):
    first_name = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.first_name
