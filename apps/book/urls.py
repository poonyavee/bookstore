from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import (
    BookCreateAPI,
    BookDetailAPI,
    AuthorCreateAPI,
)


app_name = 'book'

router = DefaultRouter(trailing_slash=False)

urlpatterns = [
    path('book', BookCreateAPI.as_view(), name='book-list'),
    path('book/<uuid:uid>', BookDetailAPI.as_view(), name='book-detail'),
    path('author', AuthorCreateAPI.as_view(), name='author-list'),
]
urlpatterns += router.urls