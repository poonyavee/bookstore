from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ..book.models import Author


class BookCreateSerializer(serializers.Serializer):
    name = serializers.CharField()
    price = serializers.DecimalField(decimal_places=2, max_digits=12)
    author = serializers.UUIDField()

    def validate_author(self, value):
        author = Author.objects.filter(uid=value).first()
        if not author:
            raise ValidationError('author is not exists.')
        return author


class BookUpdateSerializer(serializers.Serializer):
    name = serializers.CharField(required=False)
    price = serializers.DecimalField(required=False, decimal_places=2, max_digits=12)
    author = serializers.UUIDField(required=False)

    def validate_author(self, value):
        author = Author.objects.filter(uid=value).first()
        if not author:
            raise ValidationError('author is not exists.')
        return author


class BookDetailSerializer(serializers.Serializer):
    id = serializers.UUIDField(source='uid')
    name = serializers.CharField()
    price = serializers.DecimalField(decimal_places=2, max_digits=12)
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        author = obj.author
        if not author:
            return None
        return {
            'id': author.uid,
            'first_name': author.first_name,
            'last_name': author.last_name,
        }


class AuthorCreateSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=False, allow_null=True)
    last_name = serializers.CharField(required=False, allow_null=True)


class AuthorDetailSerializer(serializers.Serializer):
    id = serializers.UUIDField(source='uid')
    first_name = serializers.CharField()
    last_name = serializers.CharField()